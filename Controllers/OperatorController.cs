﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalculatorRepository.DTO;
using CalculatorRepository.Models;
using CalculatorRepository.Repository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CalculatorAPI.Controllers
{
    [Route("api/operators")]
    public class OperatorController : Controller
    {
        private readonly IDataRepository<Operator, OperatorDTO> _dataRepository;
        public OperatorController(IDataRepository<Operator, OperatorDTO> dataRepository)
        {
            _dataRepository = dataRepository;
        }
        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            var operators = _dataRepository.GetAll();
            return Ok(operators);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
